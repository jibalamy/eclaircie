.. lang:: fr

Thèmes
%%%%%%

éClaircie permet d'associer des thèmes Sphinx différents à des catégories ou des sous-catégories du blog.

Testez les thèmes disponibles en cliquant dans le menu à gauche !

.. lang:: en

Themes
%%%%%%

éClaircie allows per-category or per-document Sphinx themes.

Let's try the available themes in the menu on the left!
