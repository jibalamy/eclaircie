.. lang:: fr

Audio
=====

Les fichier audios peuvent être insérés (avec un lecteur HTML5)
avec la directive \.\. audio\:\: . Plusieurs fichiers dans des
formats différents peuvent être listés, par ordre décroissant de priorité.

Les fichiers audios sont à placer dans le répertoire "_downloads" du site.

.. lang:: en

Audio
=====

Audio files can be inserted with the \.\. audio\:\: directive. Several files with different formats
can be listed, by decreasing priority order.

Audio files must be located in the "_downloads" directory.


.. lang:: all

.. code-block:: rest

   .. audio:: amazing_grace.ogg

.. audio:: amazing_grace.ogg

