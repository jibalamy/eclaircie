.. lang:: fr

Message bilingue
================

Ceci est un exemple de message bilingue français-anglais. Utilisez les petits
drapeaux en haut à gauche de la page pour changer de langue.

La seconde partie du message inclus le code source.

.. more::

La directive \.\. lang\:\: permet de choisir la langue active, par exemple
"fr" ou "en". "all" permet d'écrire dans **toutes** les langues simultanément,
par exemple pour insérer du code ou des photos.

éClaircie utilise le caractère "=" pour souligner les titres de message.

.. lang:: en

biblingual post
===============

This is an example of bilingual post (French and English). Use the small flag at the
top-left corner of the page for selecting another langage.

The second part of the post include the source code.

.. more::

The \.\. lang\:\: directive select the active language, for example
"fr" or "en". "all" allows to write in **all** languages simultaneously,
for instance for inserting code or photos.

éClaircie uses the "=" character for underlining post titles.

.. lang:: all

.. code-block:: rest

   .. lang:: fr

   Message bilingue
   ================

   Ceci est un exemple de message bilingue français-anglais. Utilisez les petits
   drapeaux en haut à gauche de la page pour changer de langue.

   La seconde partie du message inclus le code source.

   .. more::

   La directive \.\. lang\:\: permet de choisir la langue active, par exemple
   "fr" ou "en". "all" permet d'écrire dans **toutes** les langues simultanément,
   par exemple pour insérer du code ou des photos.

   éClaircie utilise le caractère "=" pour souligner les titres de message.

   .. lang:: en

   biblingual post
   ===============

   This is an example of bilingual post (French and English). Use the small flag at the
   top-left corner of the page for selecting another langage.

   The second part of the post include the source code.

   .. more::

   The \.\. lang\:\: directive select the active language, for example
   "fr" or "en". "all" allows to write in **all** languages simultaneously,
   for instance for inserting code or photos.

   éClaircie uses the "=" character for underlining post titles.

   .. lang:: all

   .. code-block:: rest

      (code hidden to avoid an infinite recursion!)
