.. lang:: fr

Rechercher
%%%%%%%%%%

Ceci est un exemple de page de recherche.

Vous pouvez créer ce type de page en nommant la page "search.rst".
éClaircie ajoutera automatiquement les fonctions de recherche à la fin de cette page.

.. lang:: en

Search
%%%%%%

This is an example of search page.

You can create archive by naming your page "search.rst".
éClaircie will automatically add search tools at the end of this page.

