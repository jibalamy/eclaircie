.. lang:: fr
Vidéos
======

Les vidéos Youtube peuvent être insérer avec la directive \.\. youtube\:\: suivi de l'identifiant de la vidéo.

La vidéo est intégrée de manière sécurisée, de sorte à limiter le "traçage" de l'utilisateur : en
pratique, Youtube ne sera contacté qu'à partir du moment où la lecture de la vidéo commence. Si l'utilisateur
ne lit pas la vidéo, Youtube n'est pas contacté et ne peut donc pas le tracer.


.. lang:: en

Videos
======

You can insert a Youtube video with the \.\. youtube\:\: directive, followed by the video ID.

The video is integrated in a secured way, in order to limit user tracing.
Youtube will only be contacted when the video starts playing. If the user does not play the video,
Youtube is not contacted and thus cannot trace the user.


.. lang:: all

.. code-block:: rest

   .. youtube:: xcI3WyMCabA

.. youtube:: xcI3WyMCabA

