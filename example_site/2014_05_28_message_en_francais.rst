.. lang:: fr

Message en français
===================

Ce message est uniquement en français. Il n'apparaîtra pas dans la version anglaise du site.
