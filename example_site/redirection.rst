.. lang:: fr

Redirection
%%%%%%%%%%%

La directive \.\. redirect\:\: permet de créer des redirections.
Cela permet d'intégrer comme pages des liens vers d'autres sites oueb.

Note : dans ce cas, le contenu de la page (à part le titre et la redirection) importe peu
car il ne sera pas visible.


.. lang:: en

Redirection
%%%%%%%%%%%

The \.\. redirect\:\: directive allows to create redirections.
This can be used to integrate links to other website as pages in éClaircie.

Note: in this case, the content of the page (except title and redirection) does not matter,
since it will not be visible.


.. lang:: all

.. code-block:: rst

   .. redirect:: https://bitbucket.org/jibalamy/eclaircie

