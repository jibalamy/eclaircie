.. lang:: fr

Archives
%%%%%%%%

Ceci est un exemple de page d'archives.

Vous pouvez créer ce type de page en nommant la page "archives.rst".
éClaircie ajoutera automatiquement les archives à la fin de cette page.

.. lang:: en

Archives
%%%%%%%%

This is an example of archive page.

You can create archive by naming your page "archives.rst".
éClaircie will automatically add archives at the end of this page.

