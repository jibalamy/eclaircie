.. lang:: fr

Sous-catégorie
%%%%%%%%%%%%%%

Ceci est un exemple de sous-catégorie.

Voici un exemple de lien vers une catégorie : `lien vers Catégorie </category>` ou :doc:`</category>`

Voici un exemple de lien vers une page : `lien vers Audio </audio>` ou `</audio>`

Voici un exemple de lien vers un téléchargement : :download:`/_downloads/amazing_grace.ogg`

.. lang:: en

Subcategory
%%%%%%%%%%%

This is an example of subcategory.

Here is an example of link to a category : `link to Category </category>` or `</category>`

Here is an example of link to a page : `link to Audio </audio>` or `</audio>`

Here is an example of download link : :download:`/_downloads/amazing_grace.ogg`

