.. lang:: fr

Catégorie
%%%%%%%%%

Ceci est un exemple de catégorie du blog. éClaircie utilise le caractère "%" pour
souligner les titres de catégorie. Voici le code source :


.. lang:: en

Category
%%%%%%%%

This is an example of blog category. éClaircie use the "%" character for underlining
category title. Here is the source code:

.. lang:: all

.. code-block:: rest

   .. lang:: fr

   Catégorie
   %%%%%%%%%

   Ceci est un exemple de catégorie du blog. éClaircie utilise le caractère "%" pour
   souligner les titres de catégorie. Voici le code source :


   .. lang:: en

   Category
   %%%%%%%%

   This is an example of blog category. éClaircie use the "%" character for underlining
   category title. Here is the source code:

   .. lang:: all

   .. code-block:: rest

      (code hidden to avoid an infinite recursion!)
