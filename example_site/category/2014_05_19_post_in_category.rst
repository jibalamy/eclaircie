.. lang:: fr

Message de la catégorie
=======================

xxx

Un exemple de message dans la catégorie.

Notez qu'un message peut être inséré dans plusieurs catégories à l'aide de liens symboliques.


.. lang:: en

Post in the category
====================

An example of post in a category.

Note that a post can be associated with more than one category, by using symlink.

