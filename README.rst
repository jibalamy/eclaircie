éClaircie
%%%%%%%%%

**éClaircie -- Blog without clouds!**

éClaircie is a **100% static** blog engine.
The main features are:

+ Restructured Text (ReST) syntax

+ multilingual support, including failback language (ex a website with translated articles in French and the other in English)

+ comments by mail 

+ integrated search engine

+ multiple themes support (per-category)

+ full multimedia integration:

  + image gallery with miniatures

  + audio player (HTML5)

  + Youtube video (with anti-tracing protection)

+ RSS feed

+ content generation from Python scripts

+ email obfuscation

+ free software (GNU GPL v3 licence)

+ 100% static, **cloud-less computing**! With éClaircie, no need to use
  external services for managing comments (ex Disqus) or search (ex Google search bar).
  This is better because these external services proving "widgets" raise some concerns related to
  **privacy**. In fact, these widgets are often used for tracing users by the enterprises that
  propose them.

In French, "éclaircie" means "when the weather improves".

If independence, control of your personal data and respect for privacy are importants to you,
then éClaicie is for you!

For using éClaircie, you need:

+ `Python <http://python.org>`_ (3.4)

+ `Pillows <https://pypi.python.org/pypi/Pillow/>`_

